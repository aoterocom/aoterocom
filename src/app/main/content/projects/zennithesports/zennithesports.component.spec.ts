import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ZennithesportsComponent } from './zennithesports.component';

describe('ZennithesportsComponent', () => {
  let component: ZennithesportsComponent;
  let fixture: ComponentFixture<ZennithesportsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ZennithesportsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ZennithesportsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
