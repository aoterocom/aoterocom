import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AosystemsComponent } from './aosystems.component';

describe('AosystemsComponent', () => {
  let component: AosystemsComponent;
  let fixture: ComponentFixture<AosystemsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AosystemsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AosystemsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
