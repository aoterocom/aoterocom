import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {BlogComponent} from './main/content/blog/blog.component';
import {HomeComponent} from './main/content/home/home.component';
import {AoterocomComponent} from './main/content/projects/aoterocom/aoterocom.component';
import {ComunibaComponent} from './main/content/projects/comuniba/comuniba.component';
import {RianxosencabosComponent} from './main/content/projects/rianxosencabos/rianxosencabos.component';
import {CryptobotComponent} from './main/content/projects/cryptobot/cryptobot.component';
import {ZennithesportsComponent} from './main/content/projects/zennithesports/zennithesports.component';
import {AosystemsComponent} from './main/content/projects/aosystems/aosystems.component';

const appRoutes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: HomeComponent
  },
  {
    path: 'blog', component: BlogComponent
  },
  {
    path: 'projects/aoterocom', component: AoterocomComponent
  },

  {
    path: 'projects/comuniba', component: ComunibaComponent
  },
  {
    path: 'projects/rianxosencabos', component: RianxosencabosComponent
  },
  {
    path: 'projects/cryptobot', component: CryptobotComponent
  },
  {
    path: 'projects/zennithesports', component: ZennithesportsComponent
  },
  {
    path: 'projects/aoservices', component: AosystemsComponent
  },
  { path: '**',
    redirectTo: '/'}

];

@NgModule({
  imports: [
      RouterModule.forRoot(appRoutes)
  ],
  exports: [
      RouterModule
  ]
})
export class AppRoutingModule {

}
