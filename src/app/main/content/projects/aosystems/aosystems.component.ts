import { Component } from '@angular/core';
import {RoutingAnimationHelper} from '../../../../core/helpers/routing-animation.helper';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-aosystems',
  templateUrl: './aosystems.component.html',
  styleUrls: ['./aosystems.component.scss']
})
export class AosystemsComponent extends RoutingAnimationHelper {

  constructor(public translate: TranslateService) {
    super();
  }

}
