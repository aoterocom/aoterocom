import { Component } from '@angular/core';
import {RoutingAnimationHelper} from '../../../../core/helpers/routing-animation.helper';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-aoterocom',
  templateUrl: './cryptobot.component.html',
  styleUrls: ['./cryptobot.component.scss']
})
export class CryptobotComponent extends RoutingAnimationHelper {

  constructor(public translate: TranslateService) {
    super();
  }

}
