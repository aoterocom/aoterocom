import { Component } from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {RoutingAnimationHelper} from '../../../../core/helpers/routing-animation.helper';

@Component({
  selector: 'app-zennithesports',
  templateUrl: './zennithesports.component.html',
  styleUrls: ['./zennithesports.component.scss']
})
export class ZennithesportsComponent extends RoutingAnimationHelper  {

  constructor(public translate: TranslateService) {
    super();
  }

}
